<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
<title>Spring Security Example</title>
</head>
<body>
	
	<h4 style="color:red;">${logout}</h4>
	<h1>Welcome! to the login page</h1>

	<form action="./authenticate" method="post">
		<h2>
			Username:<input type="text" name="username" />
		</h2>
		<h2>
			Password:<input type="text" name="password" />
		</h2>
		
		<input type="submit" value="Sign in"/>
	</form>
	
	<a href="./register">Register</a>

</body>
</html>