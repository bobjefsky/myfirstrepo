package com.yash.springbootlogin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.yash.springbootlogin.model.User;
import com.yash.springbootlogin.model.User_detail;
import com.yash.springbootlogin.service.UserService;

@Controller
public class controller {

	@Autowired
	private UserService userService;

	@RequestMapping("/login")
	public String handler(Model model) {
		return "login";
	}

	@RequestMapping("/logout")
	public String logout(Model model) {
		model.addAttribute("logout", "You have successfully logged out!");
		return "login";
	}

	@RequestMapping("/authenticate")
	public String authenticate(Model model, @RequestParam String username, @RequestParam String password) {
		User loggedInUser = userService.authenticate(username, password);
		User_detail details = loggedInUser.getUser_detail();
		model.addAttribute("firstname", details.getFirst_name());
		model.addAttribute("lastname", details.getLast_name());
		return "welcome";
	}

	@RequestMapping("/welcome")
	public String showWelcome(Model model) {
		return "welcome";
	}

	@RequestMapping("/registeruser")
	public String registerUser(@ModelAttribute User user, @ModelAttribute User_detail user_detail) {

		user.setUser_detail(user_detail);
		user_detail.setUser(user);

		userService.saveUser(user);

		return "register";
	}

	@RequestMapping("/register")
	public String showRegister() {
		return "register";
	}
}
