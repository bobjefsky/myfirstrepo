package com.yash.springbootlogin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
// @ComponentScan({"com.yash.springbootlogin.service","com.yash.springbootlogin.controller"})
public class App {

	public static void main(String[] args) throws Throwable {
		SpringApplication.run(App.class, args);
	}

}
