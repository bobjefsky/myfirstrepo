package com.yash.springbootlogin.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.yash.springbootlogin.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

}
