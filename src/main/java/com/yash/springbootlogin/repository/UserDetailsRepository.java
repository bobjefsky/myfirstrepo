package com.yash.springbootlogin.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.yash.springbootlogin.model.User_detail;

@Repository
public interface UserDetailsRepository extends JpaRepository<User_detail, Long> {

}
