package com.yash.springbootlogin.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yash.springbootlogin.model.User;
import com.yash.springbootlogin.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public void saveUser(User user) {
		userRepository.save(user);

	}

	@Override
	public List<User> list() {
		return userRepository.findAll();
	}

	@Override
	public User authenticate(String username, String password) {
		List<User> userList = list();
		User authenticatedUser = null;

		for (User user : userList) {
			if (user.getUser_name().equals(username) && user.getPassword().equals(password)) {
				authenticatedUser = user;
			}
		}
		return authenticatedUser;
	}

}
