package com.yash.springbootlogin.service;

import java.util.List;

import com.yash.springbootlogin.model.User;

public interface UserService {

	void saveUser(User user);

	List<User> list();

	User authenticate(String username, String password);
}
